import { products } from "./product.model.js";
import {
  layThongTinTuForm,
  renderTable,
  showThongTinLenFom,
} from "./controllerProduct.js";
import { validator } from "./validator.js";

let dssp =[];

const BASE_URL = "https://62f8b78ae0564480352bf7a7.mockapi.io/";

let turnOnLoading = function () {
  document.getElementById("loading").style.display = "flex";
};

let turnOffLoading = function () {
  document.getElementById("loading").style.display = "none";
};

let renderProductsService = () => {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/product`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      dssp = res.data
      renderTable(dssp);
    })
    .catch(function (err) {
      turnOffLoading();
    });
};
renderProductsService();

//THÊM SẢN PHẨM
document.getElementById("addProducts").addEventListener("click", () => {
  let data = layThongTinTuForm();
//Validator tên
 let  isValid = validator.kiemTraRong(data.name,"tbName","Tên không được để trống!");
//Validator giá
   isValid = isValid & validator.kiemTraRong(data.price,"tbGia","Giá không được để trống!"),
   validator.kiemTraChuoiSo(data.price,"tbGia");
//Validator img
   isValid =isValid & validator.kiemTraRong(data.img,"tbHinhAnh","Hình ảnh không được để trống!")
//Validator screen
   isValid =isValid & validator.kiemTraRong(data.screen,"tbScreen","Trường này không được để trống!")
//Validator backCamera
   isValid =isValid & validator.kiemTraRong(data.backCamera,"tbBackCamera","Trường này không được để trống!");
//Validator frontCamera
   isValid =isValid & validator.kiemTraRong(data.frontCamera,"tbfrontCamera","Trường này không được để trống!");
//Validator desc
   isValid =isValid & validator.kiemTraRong(data.desc,"tbDesc","Trường này không được để trống!");
 
  if(isValid == false){
    return
  }
  let sanPham = new products(
    data.id,
    data.name,
    data.price,
    data.type,
    data.img,
    data.screen,
    data.backCamera,
    data.frontCamera,
    data.desc,
    data.MoTa
  );

  turnOnLoading();
  axios({
    url: `${BASE_URL}/product`,
    method: "POST",
    data: sanPham,
  })
    .then((res) => {
      turnOffLoading();
      $("#myModal").modal("hide");
      renderProductsService();
    })
    .catch((err) => {
      turnOffLoading();
    });
});

//XOÁ SẢN PHẨM
function xoaProduct(id ) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      renderProductsService();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}
window.xoaProduct = xoaProduct;

//SỬA SẢN PHẨM
function suaProduct(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      showThongTinLenFom(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
    });
}
window.suaProduct = suaProduct;

//CẬP NHẬT SẢN PHẨM
function capNhatProduct() {
  turnOnLoading();
  let dataSanPham = layThongTinTuForm();
//Validator tên
 let  isValid = validator.kiemTraRong(dataSanPham.name,"tbName","Tên không được để trống!");
//Validator giá
   isValid = isValid & validator.kiemTraRong(dataSanPham.price,"tbGia","Giá không được để trống!")&&
   validator.kiemTraChuoiSo(dataSanPham.price,"tbGia");
//Validator img
   isValid =isValid & validator.kiemTraRong(dataSanPham.img,"tbHinhAnh","Hình ảnh không được để trống!")
//Validator screen
   isValid =isValid & validator.kiemTraRong(dataSanPham.screen,"tbScreen","Trường này không được để trống!")
//Validator backCamera
   isValid =isValid & validator.kiemTraRong(dataSanPham.backCamera,"tbBackCamera","Trường này không được để trống!");
//Validator frontCamera
   isValid =isValid & validator.kiemTraRong(dataSanPham.frontCamera,"tbfrontCamera","Trường này không được để trống!");
//Validator desc
   isValid =isValid & validator.kiemTraRong(dataSanPham.desc,"tbDesc","Trường này không được để trống!");
 
  if(isValid == false){
    return
  }
  axios({
    url: `${BASE_URL}/product/${dataSanPham.id}`,
    method: "PUT",
    data: dataSanPham,
  })
    .then(function (res) {
      turnOffLoading();
      $("#myModal").modal("hide");
      renderProductsService();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}
window.capNhatProduct = capNhatProduct;

//RESET FORM
document.getElementById("resetForm").addEventListener("click", () => {
  document.getElementById("form").reset();
});

// Search
document.getElementById("inputTK").addEventListener("keyup",()=>{
  let inputTK = document.getElementById("inputTK").value;
  let searchList = [];
  dssp.forEach((sanPham) => {
    if(sanPham.type.match(inputTK)){
      searchList.push(sanPham)
      renderTable(searchList)
    }
    if(inputTK==""){
      renderTable(dssp)
    }
    
  });

}) 
document.getElementById("btnThemSP").onclick=()=>{
  document.getElementById("form").reset();
}