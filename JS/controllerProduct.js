export let renderTable = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let trContent = /*html */ `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.type}</td>
        <td><img src="${item.img}" style="width:80px;" alt=""/></td>
        <td>
        <p>Screen : ${item.screen}</p>
        <p>Back Camera: ${item.backCamera}</p>
        <p>Front Camera: ${item.frontCamera}</p>
        <p>Desc : ${item.desc}</p>
        </td>
        <td>
        <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick= "suaProduct(${item.id})">Sửa</button>
        <button class="btn btn-danger"  onclick= "xoaProduct(${item.id})">Xoá</button>
        </td>
        </tr>
        `;
    contentHTML += trContent;
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
  });
};

//LẤY THÔNG TIN TỪ FORM
export let layThongTinTuForm = () => {
  let id = document.getElementById("productID").value.trim() ;
  let name = document.getElementById("name").value.trim();
  let price = document.getElementById("price").value.trim();
  let type = document.getElementById("type").value.trim();
  let img = document.getElementById("img").value.trim();
  let screen = document.getElementById("screen").value.trim();
  let backCamera = document.getElementById("backCamera").value.trim();
  let frontCamera = document.getElementById("frontCamera").value.trim();
  let desc = document.getElementById("desc").value.trim();

  return {
    id,
    name,
    price,
    type,
    img,
    screen,
    backCamera,
    frontCamera,
    desc,
  };
  
};

//SHOW THÔNG TIN LÊN FORM
export let showThongTinLenFom = (sanPham) => {
  let { 
    id,
    name,
    price,
    type,
    img,
    screen,
    backCamera,
    frontCamera,
    desc,
    
    }=sanPham
    
  document.getElementById("productID").value  = id ;
  document.getElementById("name").value=name;
  document.getElementById("price").value=price;
  document.getElementById("type").value=type ;
  document.getElementById("img").value=img;
  document.getElementById("screen").value=screen;
  document.getElementById("backCamera").value=backCamera;
  document.getElementById("frontCamera").value=frontCamera;
  document.getElementById("desc").value=desc;
};
